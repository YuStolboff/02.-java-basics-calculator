package com.epam.javaBasics.calculator.solbovy;

import java.util.EmptyStackException;
import java.util.Scanner;

class Main {

    public static void main(String[] args) {
        System.out.println("Внимательно: числа, операнды и скобки разделяются пробелами!");
        System.out.print("Введите выражение: ");
        System.out.flush();
        ConverterInfixToPostfix infixToPostfix = new ConverterInfixToPostfix(scansExpression());
        Calculator calculatesPostfix = new Calculator(infixToPostfix.transPostfix().trim());
        try {
            System.out.println("Результат: " + calculatesPostfix.calculatesExpression());
        } catch (NumberFormatException e) {
            System.out.println("Проверьте вводимое выражение: содержаться недопустимые для математического" +
                    " выражения символы или числа и операнды введены без пробелов.");
        } catch (EmptyStackException e) {
            System.out.println("Калькулятор не работает с отрицательными числами");
        }
    }

    private static String scansExpression() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
